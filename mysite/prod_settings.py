from .settings import *
import dj_database_url

SECRET_KEY = '6#klvd9_-f#@bqld!)ajyt8czaxo_2f4#els#w&k*41fx5-=+5'
DEBUG = False
TEMPLATES_DEBUG = False
ALLOWED_HOSTS = ["todolist-eliam.herokuapp.com"]

DATABASES['default'] = dj_database_url.config()

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql',
#         'NAME': os.path.join(BASE_DIR, 'd7j2tlckb1461q'),
#         'USER': 'gyzcgkmvceryrv',
#         'PASSWORD': 'aa80df7d54f0bc153d516d0fd3c3e7c53336b94621cb4ea17db96f7786693d8c',
#         'HOST': 'ec2-176-34-183-20.eu-west-1.compute.amazonaws.com',
#         'PORT': '5432',

#     }
# }
# postgres://gyzcgkmvceryrv:aa80df7d54f0bc153d516d0fd3c3e7c53336b94621cb4ea17db96f7786693d8c@ec2-176-34-183-20.eu-west-1.compute.amazonaws.com:5432/d7j2tlckb1461q

MIDDLEWARE.append("whitenoise.middleware.WhiteNoiseMiddleware")
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
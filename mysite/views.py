from django.shortcuts import render, redirect

def index( request ):
    return render(request, 'index.html', {})

def redirect_tasks( request ):
    return redirect('/tasks')
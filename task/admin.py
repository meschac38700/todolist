from django.contrib import admin
from task.models import Task

class TaskAdmin(admin.ModelAdmin):

    list_display = ('title', 'completed', 'deadline', 'completed_since', 'comment')
    
admin.site.register(Task, TaskAdmin)
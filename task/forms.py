from django import forms
from .models import Task

class DateInput(forms.DateInput):
    input_type = 'date'

class TaskForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            if field != 'completed':
                self.fields[field].widget.attrs.update({
                    'class': 'form-control'
                })
            else:
                if 'initial' in kwargs:
                    default_completed = kwargs.get('initial', None).get('completed', None)
                    if default_completed:
                        self.fields[field].widget.attrs.update({
                            'checked': 'checked'
                    })
    
    class Meta:
        model = Task
        fields = ['title', 'deadline', 'completed', 'comment']
        widgets = {
            'deadline': DateInput(),
        }
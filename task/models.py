from django.db import models
import datetime
class Task(models.Model):

    title = models.CharField("Titre de la tâche à effectuer ", max_length=100)
    deadline = models.DateTimeField("Date de fin ",blank=False, null=False)
    completed = models.BooleanField("Tâche complete ? ",default=False)
    comment = models.TextField("Informations complémentaire ",null=True, blank=True)
    completed_since = models.DateTimeField('Tâche completée le ', null=True, blank=True)
    created =  models.DateTimeField(auto_now_add = True)
    updated =  models.DateTimeField(auto_now = True)
    

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):

        if self.completed:
            self.completed_since = datetime.datetime.now()
        else:
            self.completed_since = None
        super(Task, self).save(*args, **kwargs)
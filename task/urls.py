from django.urls import path
from . import views

urlpatterns = [
    path('', views.TaskListView.as_view(), name="task_all" ),
    path('view/<int:pk>', views.TaskDetailView.as_view(), name="task_view" ),
    path('add', views.add, name="task_add" ),
    path('edit/<int:pk>', views.edit, name="task_edit" ),
    path('complete/<int:pk>', views.complete, name="task_complete" ),
    path('delete/<int:pk>', views.delete, name="task_delete" ),
]

from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from .models import Task
from .forms import TaskForm

class TaskListView(ListView):
    model = Task
    template_name = "task/index.html"
    context_object_name = 'tasks'

class TaskDetailView(DetailView):
    model = Task
    template_name = "task/view.html"
    context_object_name = 'task'

def add(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('task_all')
    else:
        form = TaskForm()
    return render(request, "task/add.html", {'form': form } )
    

def edit(request, pk):
    obj = get_object_or_404(Task, pk=pk)
    # test = TaskForm(obj.completed ,initial={'title': obj.title, 'deadline': obj.deadline, 'comment': obj.comment })

    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            obj.title = form.data['title']
            if 'completed' in form.data.keys() :
                obj.completed = True if form.data['completed'] == 'on' else False
            else:
                obj.completed = False
            obj.comment = form.data['comment']
            obj.deadline = form.data['deadline']
            obj.save()
            return redirect('task_all')
    else :
        form = TaskForm(initial={'completed': obj.completed,'title': obj.title, 'deadline': obj.deadline, 'comment': obj.comment  })
    return render(request, "task/edit.html", {'form': form } )
    

def complete(request, pk):
    obj = get_object_or_404(Task, pk=pk)
    print(obj.completed)
    obj.completed = True
    print(obj.completed)
    obj.save()
    return redirect('task_all')

def delete(request, pk):
    obj = get_object_or_404(Task, pk=pk)
    obj.delete()
    return redirect('task_all')